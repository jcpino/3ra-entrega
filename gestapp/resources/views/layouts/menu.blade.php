@canany(['edit_books','delete_books','view_books','create_books'])
<li class="{{ Request::is('books*') ? 'active' : '' }}">
    <a href="{{ route('books.index') }}"><i class="fa fa-edit"></i><span>Books</span></a>
</li>
@endcan

@canany(['edit_roles','delete_roles','view_roles','create_roles'])
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{{ route('roles.index') }}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>
@endcan
@canany(['edit_users','delete_users','view_users','create_users'])
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>
@endcan
@canany(['edit_tender','delete_tender','view_tender','create_tender'])
<li class="{{ Request::is('tenders*') ? 'active' : '' }}">
    <a href="{{ route('tenders.index') }}"><i class="fa fa-edit"></i><span>Tenders</span></a>
</li>
@endcan
@canany(['edit_apertura','delete_apertura','view_apertura','create_apertura'])
<li class="{{ Request::is('aperturas*') ? 'active' : '' }}">
    <a href="{{ route('aperturas.index') }}"><i class="fa fa-edit"></i><span>Aperturas</span></a>
</li>
@endcan
