<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $apertura->type }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $apertura->status }}</p>
</div>

<!-- Column Field -->
<div class="form-group">
    {!! Form::label('column', 'Column:') !!}
    <p>{{ $apertura->column }}</p>
</div>

<!-- Totalpagado Field -->
<div class="form-group">
    {!! Form::label('totalpagado', 'Totalpagado:') !!}
    <p>{{ $apertura->totalpagado }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $apertura->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $apertura->updated_at }}</p>
</div>

