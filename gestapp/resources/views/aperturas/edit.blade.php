@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Apertura
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($apertura, ['route' => ['aperturas.update', $apertura->id], 'method' => 'patch']) !!}

                        @include('aperturas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection