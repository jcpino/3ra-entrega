<div class="table-responsive">
    <table class="table" id="aperturas-table">
        <thead>
            <tr>
                <th>Type</th>
        <th>Status</th>
        <th>Column</th>
        <th>Totalpagado</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($aperturas as $apertura)
            <tr>
                <td>{{ $apertura->type }}</td>
            <td>{{ $apertura->status }}</td>
            <td>{{ $apertura->column }}</td>
            <td>{{ $apertura->totalpagado }}</td>
                <td>
                    {!! Form::open(['route' => ['aperturas.destroy', $apertura->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    @can('view_apertura')
                       <a href="{{ route('aperturas.show', [$apertura->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                       @endcan
                       @can('edit_apertura')
                        <a href="{{ route('aperturas.edit', [$apertura->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('delete_apertura')
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
