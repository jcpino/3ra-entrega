<div class="table-responsive">
    <table class="table" id="tenders-table">
        <thead>
            <tr>
                <th>Concept</th>
        <th>Ccy</th>
        <th>Amount</th>
        <th>Xrate</th>
        <th>Usdep</th>
        <th>Comentarios</th>
        <th>Proveedor</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tenders as $tender)
            <tr>
                <td>{{ $tender->concept }}</td>
            <td>{{ $tender->ccy }}</td>
            <td>{{ $tender->amount }}</td>
            <td>{{ $tender->xrate }}</td>
            <td>{{ $tender->usdep }}</td>
            <td>{{ $tender->comentarios }}</td>
            <td>{{ $tender->proveedor }}</td>
                <td>
                    {!! Form::open(['route' => ['tenders.destroy', $tender->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    @can('view_tender')
                        <a href="{{ route('tenders.show', [$tender->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('edit_tender')
                        <a href="{{ route('tenders.edit', [$tender->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('delete_tender')
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
