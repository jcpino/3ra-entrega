<!-- Concept Field -->
<div class="form-group">
    {!! Form::label('concept', 'Concept:') !!}
    <p>{{ $tender->concept }}</p>
</div>

<!-- Ccy Field -->
<div class="form-group">
    {!! Form::label('ccy', 'Ccy:') !!}
    <p>{{ $tender->ccy }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $tender->amount }}</p>
</div>

<!-- Xrate Field -->
<div class="form-group">
    {!! Form::label('xrate', 'Xrate:') !!}
    <p>{{ $tender->xrate }}</p>
</div>

<!-- Usdep Field -->
<div class="form-group">
    {!! Form::label('usdep', 'Usdep:') !!}
    <p>{{ $tender->usdep }}</p>
</div>

<!-- Comentarios Field -->
<div class="form-group">
    {!! Form::label('comentarios', 'Comentarios:') !!}
    <p>{{ $tender->comentarios }}</p>
</div>

<!-- Proveedor Field -->
<div class="form-group">
    {!! Form::label('proveedor', 'Proveedor:') !!}
    <p>{{ $tender->proveedor }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $tender->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $tender->updated_at }}</p>
</div>

