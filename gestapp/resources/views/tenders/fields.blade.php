<!-- Concept Field -->
<div class="form-group col-sm-6">
    {!! Form::label('concept', 'Concept:') !!}
    {!! Form::text('concept', null, ['class' => 'form-control']) !!}
</div>

<!-- Ccy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ccy', 'Ccy:') !!}
    {!! Form::text('ccy', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Xrate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('xrate', 'Xrate:') !!}
    {!! Form::text('xrate', null, ['class' => 'form-control']) !!}
</div>

<!-- Usdep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usdep', 'Usdep:') !!}
    {!! Form::text('usdep', null, ['class' => 'form-control']) !!}
</div>

<!-- Comentarios Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comentarios', 'Comentarios:') !!}
    {!! Form::text('comentarios', null, ['class' => 'form-control']) !!}
</div>

<!-- Proveedor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('proveedor', 'Proveedor:') !!}
    {!! Form::text('proveedor', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tenders.index') }}" class="btn btn-default">Cancel</a>
</div>
