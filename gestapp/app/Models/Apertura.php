<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Apertura
 * @package App\Models
 * @version November 29, 2021, 8:14 pm UTC
 *
 * @property string $type
 * @property string $status
 * @property string $column
 * @property string $totalpagado
 */
class Apertura extends Model
{
    use SoftDeletes;

    public $table = 'aperturas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'type',
        'status',
        'column',
        'totalpagado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string',
        'status' => 'string',
        'column' => 'string',
        'totalpagado' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
        'status' => 'required',
        'column' => 'required',
        'totalpagado' => 'required'
    ];

    
}
