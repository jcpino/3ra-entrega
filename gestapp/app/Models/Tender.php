<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tender
 * @package App\Models
 * @version November 29, 2021, 8:03 pm UTC
 *
 * @property string $concept
 * @property string $ccy
 * @property string $amount
 * @property string $xrate
 * @property string $usdep
 * @property string $comentarios
 * @property string $proveedor
 */
class Tender extends Model
{
    use SoftDeletes;

    public $table = 'tenders';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'concept',
        'ccy',
        'amount',
        'xrate',
        'usdep',
        'comentarios',
        'proveedor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'concept' => 'string',
        'ccy' => 'string',
        'amount' => 'string',
        'xrate' => 'string',
        'usdep' => 'string',
        'comentarios' => 'string',
        'proveedor' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'concept' => 'required',
        'ccy' => 'required',
        'amount' => 'required',
        'xrate' => 'required',
        'usdep' => 'required',
        'comentarios' => 'required',
        'proveedor' => 'required'
    ];

    
}
