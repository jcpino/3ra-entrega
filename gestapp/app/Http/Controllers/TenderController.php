<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTenderRequest;
use App\Http\Requests\UpdateTenderRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Tender;
use Illuminate\Http\Request;
use Flash;
use Response;

class TenderController extends AppBaseController
{
    /**
     * Display a listing of the Tender.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Tender $tenders */
        $tenders = Tender::all();

        return view('tenders.index')
            ->with('tenders', $tenders);
    }

    /**
     * Show the form for creating a new Tender.
     *
     * @return Response
     */
    public function create()
    {
        return view('tenders.create');
    }

    /**
     * Store a newly created Tender in storage.
     *
     * @param CreateTenderRequest $request
     *
     * @return Response
     */
    public function store(CreateTenderRequest $request)
    {
        $input = $request->all();

        /** @var Tender $tender */
        $tender = Tender::create($input);

        Flash::success('Tender saved successfully.');

        return redirect(route('tenders.index'));
    }

    /**
     * Display the specified Tender.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tender $tender */
        $tender = Tender::find($id);

        if (empty($tender)) {
            Flash::error('Tender not found');

            return redirect(route('tenders.index'));
        }

        return view('tenders.show')->with('tender', $tender);
    }

    /**
     * Show the form for editing the specified Tender.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Tender $tender */
        $tender = Tender::find($id);

        if (empty($tender)) {
            Flash::error('Tender not found');

            return redirect(route('tenders.index'));
        }

        return view('tenders.edit')->with('tender', $tender);
    }

    /**
     * Update the specified Tender in storage.
     *
     * @param int $id
     * @param UpdateTenderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTenderRequest $request)
    {
        /** @var Tender $tender */
        $tender = Tender::find($id);

        if (empty($tender)) {
            Flash::error('Tender not found');

            return redirect(route('tenders.index'));
        }

        $tender->fill($request->all());
        $tender->save();

        Flash::success('Tender updated successfully.');

        return redirect(route('tenders.index'));
    }

    /**
     * Remove the specified Tender from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tender $tender */
        $tender = Tender::find($id);

        if (empty($tender)) {
            Flash::error('Tender not found');

            return redirect(route('tenders.index'));
        }

        $tender->delete();

        Flash::success('Tender deleted successfully.');

        return redirect(route('tenders.index'));
    }
}
