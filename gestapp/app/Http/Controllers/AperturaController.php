<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAperturaRequest;
use App\Http\Requests\UpdateAperturaRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Apertura;
use Illuminate\Http\Request;
use Flash;
use Response;

class AperturaController extends AppBaseController
{
    /**
     * Display a listing of the Apertura.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Apertura $aperturas */
        $aperturas = Apertura::all();

        return view('aperturas.index')
            ->with('aperturas', $aperturas);
    }

    /**
     * Show the form for creating a new Apertura.
     *
     * @return Response
     */
    public function create()
    {
        return view('aperturas.create');
    }

    /**
     * Store a newly created Apertura in storage.
     *
     * @param CreateAperturaRequest $request
     *
     * @return Response
     */
    public function store(CreateAperturaRequest $request)
    {
        $input = $request->all();

        /** @var Apertura $apertura */
        $apertura = Apertura::create($input);

        Flash::success('Apertura saved successfully.');

        return redirect(route('aperturas.index'));
    }

    /**
     * Display the specified Apertura.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Apertura $apertura */
        $apertura = Apertura::find($id);

        if (empty($apertura)) {
            Flash::error('Apertura not found');

            return redirect(route('aperturas.index'));
        }

        return view('aperturas.show')->with('apertura', $apertura);
    }

    /**
     * Show the form for editing the specified Apertura.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Apertura $apertura */
        $apertura = Apertura::find($id);

        if (empty($apertura)) {
            Flash::error('Apertura not found');

            return redirect(route('aperturas.index'));
        }

        return view('aperturas.edit')->with('apertura', $apertura);
    }

    /**
     * Update the specified Apertura in storage.
     *
     * @param int $id
     * @param UpdateAperturaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAperturaRequest $request)
    {
        /** @var Apertura $apertura */
        $apertura = Apertura::find($id);

        if (empty($apertura)) {
            Flash::error('Apertura not found');

            return redirect(route('aperturas.index'));
        }

        $apertura->fill($request->all());
        $apertura->save();

        Flash::success('Apertura updated successfully.');

        return redirect(route('aperturas.index'));
    }

    /**
     * Remove the specified Apertura from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Apertura $apertura */
        $apertura = Apertura::find($id);

        if (empty($apertura)) {
            Flash::error('Apertura not found');

            return redirect(route('aperturas.index'));
        }

        $apertura->delete();

        Flash::success('Apertura deleted successfully.');

        return redirect(route('aperturas.index'));
    }
}
