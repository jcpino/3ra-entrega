<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tender;
use Faker\Generator as Faker;

$factory->define(Tender::class, function (Faker $faker) {

    return [
        'concept' => $faker->word,
        'ccy' => $faker->word,
        'amount' => $faker->word,
        'xrate' => $faker->word,
        'usdep' => $faker->word,
        'comentarios' => $faker->word,
        'proveedor' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
