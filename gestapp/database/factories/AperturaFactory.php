<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Apertura;
use Faker\Generator as Faker;

$factory->define(Apertura::class, function (Faker $faker) {

    return [
        'type' => $faker->word,
        'status' => $faker->word,
        'column' => $faker->word,
        'totalpagado' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
